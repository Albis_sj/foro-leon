export interface UsuarioI {
  usuario: string;
  password: string;
}

export interface UsuarioRegistradoI {
  nombre: string,
  email: string,
  usuario: string,
  genero: string,
  contraseña1: string,
  contraseña2: string
}

export interface foroI {
  foroid: number,
  usuario: String,
  seccion: number,
  titulo: string,
  fecha: string,
  foro: string,
}

export interface comentariosI {
  comentid : number,
  foroid: number,
  usuario: string,
  fecha: fechaI,
  comentario: string,
}

interface fechaI {
  dia: string, 
  hora: string
}