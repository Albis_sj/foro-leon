import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { CrearForoComponent } from './foros/crear-foro/crear-foro.component';
import { ForosComponent } from './foros/foros.component';
import { VerForoComponent } from './foros/ver-foro/ver-foro.component';
import { InicioComponent } from './inicio/inicio.component';

const routes: Routes = [
  { path: '', component: DashboardComponent,
    children:
  [
    { path: '', component: InicioComponent },
    { path: 'foros', component: ForosComponent },
    { path: 'crear-usuario/:id', component: CrearForoComponent },
    { path: 'ver-usuario/:id', component: VerForoComponent },
  ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
