import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userName!: string;
  mostrar = false
  constructor(private _usuariosSvc : UsuariosService,
              private router : Router){
    // this.userName = this._usuariosSvc.nombre_usuario

    let nombre = sessionStorage.getItem('Usuario_name')
if (nombre === null){
  this.userName = ''
} else {
  this.userName = nombre
  this.mostrar = true
}
    
    console.log(nombre);
  }

  ngOnInit(): void {
  }  

  guardar():void{
  }
  
  goToInicioSession(){
    this.router.navigate(['/login'])
  }

  cerrarSesion(){
    this._usuariosSvc.cerrarSesion();
    this.router.navigate(['/dashboard'])
  }
}
